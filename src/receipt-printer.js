export default class ReceiptPrinter {
  constructor (products) {
    this.products = [...products];
  }

  print (barcodes) {
    // TODO: Please implement the method
    // <-start-
    var receiptDetails = '\n';
    if (barcodes.length === 0) {
      receiptDetails = '\n\n';
    }
    var receiptTotalPrice = 0;

    const productDictionary = barcodes.reduce((obj, barcode) => {
      if (obj[barcode] !== undefined) {
        obj[barcode].number += 1;
      } else {
        var searchStatus = false;
        this.products.forEach(product => {
          if (product.barcode === barcode) {
            obj[barcode] = {
              product: product,
              number: 1
            };
            obj[barcode].number = 1;
            searchStatus = true;
            return obj;
          }
        });
        if (!searchStatus) {
          throw Error('Unknown barcode.');
        }
      }
      return obj;
    }, {});

    Object.keys(productDictionary).sort().reverse().forEach(barcode => {
      var appendWidth1 = 'Coca Cola                     '.length - productDictionary[barcode].product.name.length;
      var appendWidth2 = 'x1        '.length - 2;
      var nameSection = productDictionary[barcode].product.name;
      var numberSection = `x${productDictionary[barcode].number}`;
      for (let i = 0; i < appendWidth1; i++) {
        nameSection += ' ';
      }
      for (let i = 0; i < appendWidth2; i++) {
        numberSection += ' ';
      }
      receiptDetails = (nameSection + numberSection + productDictionary[barcode].product.unit + '\n') + receiptDetails;
      receiptTotalPrice += productDictionary[barcode].product.price * productDictionary[barcode].number;
    });
    return `==================== Receipt ====================\n${receiptDetails}===================== Total =====================\n${receiptTotalPrice.toFixed(2)}`;
    // --end->
  }
}
