## Business

In this test, you will implement `print` method of `ReceiptPrinter` class. This method accepts a collection of barcode strings. You should extract the information from the `barcodes` parameters as well as `products` in order to create the receipt.

For more details, please read the tests. Please note that you **CAN ONLY** modify code within the range defined by `<-start-` and `--end->`.

## Definition of *Done*

* No additional 3rd party libraries are allowed, esp. *lodash* and *underscore*.
* All unit tests are passed.
* There is not *linting* error.
* Good code quality.
  * Meaningful names of `class`, variable and functions.
  * Using full name rather than abbreviation.
  * Using appropriate data structure.

## Hint

### Running Unit Test

If you want to run all the unit test, you can run:

```bash
$ npm test
```

If you want to run test which matches certain patterns, you can run:

```bash
$ npm test -- <pattern>
```

For example.

```bash
$ npm test -- receptionist
```

### Linting Your Code

You can use the following command to check if your code violates the coding standard:

```bash
$ npm run lint
```
